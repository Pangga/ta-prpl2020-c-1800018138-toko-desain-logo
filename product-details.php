<?php include "db.php";
$x=$_GET["a"];?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Product Details</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/favicon.ico">

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="scss/box.css">
	<style> 
		input[type=submit]{
		  background-color: black;
		  border: 0;
		  border-radius: 5px;
		  cursor: pointer;
		  color: #fff;
		  font-size:16px;
		  font-weight: bold;
		  line-height: 1.4;
		  padding: 10px 32px;
		}
	</style>
</head>

<body style="background-color:#1C1B1B">
	<div id="bg"></div>
	<div id="modal-kotak">
		<div id="atas">
			<table border="2px">
				<tr>
					<td>Masih belum di buat</td>
					<td>Lagi Males nanti aja</td>
				</tr>
			</table>
		</div>
		<div id="bawah">
			<button id="tombol-tutup">CLOSE</button>
		</div>
	</div>
	<!-- Product Details Area -->
	
    <!-- Search Wrapper Area Start -->
    <div class="search-wrapper section-padding-100">
        <div class="search-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-content">
                        <form action="#" method="get">
                            <input type="search" name="search" id="search" placeholder="Type your keyword...">
                            <button type="submit"><img src="img/core-img/search.png" alt=""></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Search Wrapper Area End -->

    <!-- ##### Main Content Wrapper Start ##### -->
    <div class="main-content-wrapper d-flex clearfix">

        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>

        <!-- Header Area Start -->
        <header class="header-area clearfix" style="background: red">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
            </div>
			<div style="display: block">
				<?php
					if($_SERVER["REQUEST_METHOD"]=="GET"){
						if(empty($_GET["loc"])){

						}
						else{
							$akunid=$_GET["loc"];
						}
					}
					if($akunid<0 || $akunid==" "){
					?><form action="akun.php"><input type="submit" value="Login Here!" onClick="akun.php"></form><?php
					}else{
						?>
						<p style="color: white">WELCOME <?php echo strtoupper($user[$akunid]);?><br>
						<a href="product-details.php?a=<?php echo $x;?>" style="color: black">log out!!</a></p>
					<?php
					}
				?>
			</div><br>
            <!-- Amado Nav -->
            <nav class="amado-nav">
                <ul>
                    <li><a href="index.html" style="color: white">Home</a></li>
                    <li><a href="shop.php" style="color: white">Shop</a></li>
                    <li class="active" ><a href="product-details.php" style="color: white">Product</a></li>
                </ul>
            </nav>

            <!-- Social Button -->
            <div class="social-info d-flex justify-content-between">
                <a href="https://id.pinterest.com/wolnkov/"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/wolnkov/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://web.facebook.com/Wolnkov?_rdc=1&_rdr"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>
        </header>
        <!-- Header Area End -->
		
		
		<!-- Menampilkan Product -->
		<?php 
		for($i=0;$i<$jumlah_data;$i++){
			if($x==$id[$i]){
		?>
        <!-- Product Details Area Start -->
        <div class="single-product-area section-padding-100 clearfix">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-12">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb mt-50">
                                <li class="breadcrumb-item"><a href="index.html" style="color: white">Home</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12 col-lg-7">
                        <div class="single_product_thumb">
                            <div id="product_details_slider" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
									<?php 
									$status="";
									for($p=0;$p<$jml[$x-1];$p++){ ?>
									<?php
										if($p==0){
											$status="active";
										}else{ $status="";}
									?>
                                    <li class="<?php echo $status?>" data-target="#product_details_slider" data-slide-to="<?php echo $p?>" style="background-image: url(img/product-img/<?php echo $x?>/pro<?php echo $p+1?>.jpg);">
                                    </li>
									<?php }?>
                                </ol>
                                <div class="carousel-inner">
									<?php 
									for($p=0;$p<$jml[$x-1];$p++){ ?>
									<?php
										if($p==0){
											$status="active";
										}else{ $status="";}
									?>
                                    <div class="carousel-item <?php echo $status?>">
                                        <a class="gallery_img" href="img/product-img/<?php echo $x?>/pro<?php echo $p+1?>.jpg">
                                            <img class="d-block w-100" src="img/product-img/<?php echo $x?>/pro<?php echo $p+1?>.jpg" alt="First slide">
                                        </a>
                                    </div>
									<?php }?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-5">
                        <div class="single_product_desc">
                            <!-- Product Meta Data -->
                            <div class="product-meta-data">
                                <div class="line"></div>
                                <p class="product-price"><?php echo $harga[$x-1]?></p>
                                <h2 style="color:white"><?php echo $nama[$x-1]?></h2>
                                <!-- Ratings & Review -->
                                <div class="ratings-review mb-15 d-flex align-items-center justify-content-between">
                                    <div class="ratings">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                </div>
                                <!-- Avaiable -->
                                <p class="avaibility"><i class="fa fa-circle"></i> In Stock</p>
                            </div>

                            <div class="short_overview my-5">
                                <p><?php echo $ket[$x-1]?></p>
                            </div>
         
                                <a id="tombol" type="submit" class="btn amado-btn"><p style="color: white; padding: 1px">BUY</p></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product Details Area End -->
		<?php }
		}?>
		
	
		
    </div>
    <!-- ##### Main Content Wrapper End ##### -->
	
	
	


    <!-- ##### Footer Area Start ##### -->
    <footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">	
                        <!-- Copywrite Text -->
                        <p class="copywrite"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">Wolnkov Design</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						</div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <div class="footer_menu"></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
			$('#tombol').click(function(){
				$('#modal-kotak , #bg').fadeIn("slow");
			});
			$('#tombol-tutup').click(function(){
				$('#modal-kotak , #bg').fadeOut("slow");
			});		
		});
	</script>

</body>

</html>