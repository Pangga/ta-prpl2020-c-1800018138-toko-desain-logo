<?php include "db.php";
$halaman=0;
if($_SERVER["REQUEST_METHOD"]=="GET"){
	if(empty($_GET["halaman"])){
		
	}
	else{
		$halaman=$_GET["halaman"];
	}
}
$status1=$status2=$status3=$status4=" ";
if($halaman==0){$status1="active";}
else if($halaman==6){$status2="active";}
else if($halaman==12){$status3="active";}
else if($halaman==18){$status4="active";}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- The above 4 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <!-- Title  -->
    <title>Wolnkov Design Shop</title>

    <!-- Favicon  -->
    <link rel="icon" href="img/core-img/favicon.ico">
	<!-- Jquery-->
	<script type="text/javascript" src="js/jquery.js"></script>

    <!-- Core Style CSS -->
    <link rel="stylesheet" href="css/core-style.css">
    <link rel="stylesheet" href="style.css">
	<link rel="stylesheet" href="scss/box.css">
	<style> 
	input[type=submit]{
	  background-color: red;
	  border: 0;
	  border-radius: 5px;
	  cursor: pointer;
	  color: #fff;
	  font-size:16px;
	  font-weight: bold;
	  line-height: 1.4;
	  padding: 10px 32px;
	}
	</style>

</head>

<body style="background-color:#1C1B1B">	
    <!-- Search Wrapper Area Start -->
    <div class="search-wrapper section-padding-100">
        <div class="search-close">
            <i class="fa fa-close" aria-hidden="true"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="search-content">
                        <form action="#" method="get">
                            <input type="search" name="search" id="search" placeholder="Type your keyword...">
                            <button type="submit"><img src="img/core-img/search.png" alt=""></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <div class="main-content-wrapper d-flex clearfix">
        <!-- Mobile Nav (max width 767px)-->
        <div class="mobile-nav">
            <!-- Navbar Brand -->
            <div class="amado-navbar-brand">
                <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>
            </div>
            <!-- Navbar Toggler -->
            <div class="amado-navbar-toggler">
                <span></span><span></span><span></span>
            </div>
        </div>

        <!-- Header Area Start -->
        <header class="header-area clearfix" style="background-color: black;">
            <!-- Close Icon -->
            <div class="nav-close">
                <i class="fa fa-close" aria-hidden="true"></i>
            </div>
            <!-- Logo -->
            <div class="logo">
                <a href="index.html"><img src="img/core-img/logo.png" alt=""></a>				
            </div>
			<div style="display: block">
				<?php
					if($_SERVER["REQUEST_METHOD"]=="GET"){
						if(empty($_GET["loc"])){

						}
						else{
							$akunid=$_GET["loc"];
						}
					}
					if($akunid==" "){
					?><form action="akun.php"><input type="submit" value="Login Here!" onClick="akun.php"></form><?php
					}else{
						?>
						<p style="color: white">WELCOME <?php echo strtoupper($user[$akunid-1]);?><br>
						<a href="shop.php?loc=" style="color: red">log out!!</a></p>
					<?php
					}
				?>
			</div><br>
            <!-- Amado Nav -->
            <nav class="amado-nav">
                <ul>
                    <li><a href="index.html" style="color:white">Home</a></li>
                    <li class="active"><a href="shop.html" style="color:white">Shop</a></li>
                    <li><a href="product-details.php?a=1" style="color:white">Product</a></li>
                </ul>
            </nav>
            <!-- Button Group --><!-- Cart Menu --><!-- Social Button -->
            <div class="social-info d-flex justify-content-between">
                <a href="https://id.pinterest.com/wolnkov/"><i class="fa fa-pinterest" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/wolnkov/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                <a href="https://web.facebook.com/Wolnkov?_rdc=1&_rdr"><i class="fa fa-facebook" aria-hidden="true"></i></a>
            </div>
        </header>
        <!-- Header Area End -->

        <div class="shop_sidebar_area" style="background-color: red">

            <div class="widget catagory mb-50">
            </div>
        </div>
        <div class="amado_product_area section-padding-100">
			<h1 style="color: white;padding-bottom: 20px">WELCOME TO WONLKOV DESIGN</h1>
            <div class="container-fluid">                
                <div class="row">
                    <!-- Product Area -->
					<?php for($i=1+$halaman;$i<=6+$halaman;$i++){ ?>
                    <div class="col-12 col-sm-6 col-md-12 col-xl-6">
                        <div class="single-product-wrapper">
                            <!-- Product Image -->
                            <div class="product-img">
                                <img src="img/product-img/<?php echo $id[$i-1]?>/product<?php echo $a?>.jpg" alt="">
                                <!-- Hover Thumb -->
                                <img class="hover-img" src="img/product-img/<?php echo $id[$i-1]?>/product<?php echo $a+1?>.jpg" alt="">
                            </div>

                            <!-- Product Description -->
                            <div class="product-description d-flex align-items-center justify-content-between">
                                <!-- Product Meta Data -->
                                <div class="product-meta-data">
                                    <div class="line"></div>
                                    <p class="product-price"><?php echo $harga[$i-1]?></p>
                                    <a href="product-details.php?a=<?php echo $i?>&loc=<?php echo $akunid-1;?>">
                                        <h6><?php echo $nama[$i-1]?></h6>
                                    </a>
                                </div>
                                <!-- Ratings & Cart -->
                                <div class="ratings-cart text-right">
                                    <div class="ratings">
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                        <i class="fa fa-star" aria-hidden="true"></i>
                                    </div>
                                    <div class="cart">
                                        <a data-toggle="tooltip" data-placement="left" title="Buy" href="product-details.php?a=<?php echo $i?>&loc=<?php echo $akunid-1;?>"><img src="img/core-img/cart.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
					<?php }?>
                </div>

                <div class="row">
                    <div class="col-12">
                        <!-- Pagination -->
                        <nav aria-label="navigation">
                            <ul class="pagination justify-content-end mt-50">
                                <li class="page-item <?php echo $status1?>"><a class="page-link" href="shop.php">01.</a></li>
                                <li class="page-item <?php echo $status2?>"><a class="page-link" href="shop.php?halaman=6">02.</a></li>
                                <li class="page-item <?php echo $status3?>"><a class="page-link" href="shop.php?halaman=12">03.</a></li>
                                <li class="page-item <?php echo $status4?>"><a class="page-link" href="shop.php?halaman=18">04.</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
    <footer class="footer_area clearfix">
        <div class="container">
            <div class="row align-items-center">
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-4">
                    <div class="single_widget_area">	
                        <!-- Copywrite Text -->
                        <p class="copywrite"><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="#" target="_blank">Wolnkov Design</a>
							<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
						</div>
                </div>
                <!-- Single Widget Area -->
                <div class="col-12 col-lg-8">
                    <div class="single_widget_area">
                        <!-- Footer Menu -->
                        <div class="footer_menu"></div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- ##### Footer Area End ##### -->

    <!-- ##### jQuery (Necessary for All JavaScript Plugins) ##### -->
    <script src="js/jquery/jquery-2.2.4.min.js"></script>
    <!-- Popper js -->
    <script src="js/popper.min.js"></script>
    <!-- Bootstrap js -->
    <script src="js/bootstrap.min.js"></script>
    <!-- Plugins js -->
    <script src="js/plugins.js"></script>
    <!-- Active js -->
    <script src="js/active.js"></script>
</body>

</html>